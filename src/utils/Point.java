package utils;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Point implements Serializable {
	private int x;
	private int y;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public Point(Point p) { this(p.x, p.y); }
	public Point(int a) { this(a,a); }
	public Point() { this(0); }
	
	public int getX() { return this.x; }
	public int getY() { return this.y; }
	
	public Point set(Point p) { this.x = p.x; this.y = p.y; return this; }
	public Point set(int x, int y) { this.x = x; this.y = y; return this; }
	public Point setX(int x) { this.x = x; return this; }
	public Point setY(int y) { this.y = y; return this; }
	
	public Point add(Point p) { return new Point(this.x+p.x, this.y+p.y); }
	public Point sub(Point p) { return new Point(this.x-p.x, this.y-p.y); }
	public Point mul(Point p) { return new Point(this.x*p.x, this.y*p.y); }
	public Point div(Point p) { return new Point(this.x/p.x, this.y/p.y); }
	
	public Point add(int x, int y) { return new Point(this.x+x, this.y+y); }
	public Point add(int a)        { return new Point(this.x+a, this.y+a); }
	public Point sub(int x, int y) { return new Point(this.x-x, this.y-y); }
	public Point sub(int a)        { return new Point(this.x-a, this.y-a); }
	public Point mul(int x, int y) { return new Point(this.x*x, this.y*y); }
	public Point mul(int a)        { return new Point(this.x*a, this.y*a); }
	public Point mul(double a)     { return new Point((int)(this.x*a), (int)(this.y*a)); }
	public Point div(int x, int y) { return new Point(this.x/x, this.y/y); }
	public Point div(int a)        { return new Point(this.x/a, this.y/a); }
	
	public double distance(Point p) {
		double dx = x - p.x;
		double dy = y - p.y;
		return Math.sqrt(dx*dx + dy*dy);
	}
	
	public Point rotateAround(Point p, double angle) {
		angle = Math.toRadians(angle);
		int dx = this.x - p.x;
		int dy = this.y - p.y;
		this.x = (int) (dx*Math.cos(angle) - dy*Math.sin(angle) + p.x);
		this.y = (int) (dx*Math.sin(angle) + dy*Math.cos(angle) + p.y);
		return this;
	}
	
	public String toString() { return this.x+","+this.y; }
}
