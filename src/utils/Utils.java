package utils;

public class Utils {
	
	public static double NRound(double a, int n) {
		int tens = 1;
		while (n-->0) tens *= 10;
		return (double)((int)(a*tens))/tens;
	}
	
	public static int min(int a, int b) { return a>b ? b : a; }
	public static int max(int a, int b) { return a<b ? b : a; }
	public static int clamp(int value, int min, int max) { return min(max(value,min),max); }
	
}
