package objects;

import java.awt.Color;
import java.awt.Shape;
import java.io.Serializable;

import utils.Point;

@SuppressWarnings("serial")
public class RDOFile implements Serializable {
	private Color color;
	private String name;
	private Point position;
	private int angle;
	private Shape shape;
	private boolean fixed;
	
	public RDOFile(Object o) {
		this.setName(o.getName());
		this.setColor(o.getColor());
		this.setPosition(o.getPosition());
		this.setAngle(o.getAngle());
		this.setShape(o.getShape());
		this.setFixed(o.isFixed());
	}

	public Color getColor() { return color; }
	public void setColor(Color color) { this.color = color; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public Point getPosition() { return position; }
	public void setPosition(Point position) { this.position = position; }

	public int getAngle() { return angle; }
	public void setAngle(int angle) { this.angle = angle; }
	
	public Shape getShape() { return shape; }
	public void setShape(Shape shape) { this.shape = shape; }

	public boolean isFixed() { return fixed; }
	public void setFixed(boolean fixed) { this.fixed = fixed; }
}
