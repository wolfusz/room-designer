package objects;

import java.io.Serializable;
import java.util.LinkedList;

@SuppressWarnings("serial")
public class RDLFile implements Serializable {
	private int x;
	private int y;
	private int w;
	private int h;
	private LinkedList<RDOFile> objects;
	
	public RDLFile(int x, int y, int w, int h, LinkedList<Object> o) {
		this.setX(x);
		this.setY(y);
		this.setWidth(w);
		this.setHeight(h);
		this.setObjects(o);
	}

	public int getX() { return x; }
	public void setX(int x) { this.x = x; }
	public int getY() { return y; }
	public void setY(int y) { this.y = y; }

	public int getWidth() { return w; }
	public void setWidth(int w) { this.w = w; }
	public int getHeight() { return h; }
	public void setHeight(int h) { this.h = h; }

	public LinkedList<Object> getObjects() {
		LinkedList<Object> objects = new LinkedList<>();
		for (RDOFile o : this.objects)
			objects.add(new Object(o));
		return objects;
	}
	public void setObjects(LinkedList<Object> objects) {
		this.objects = new LinkedList<>();
		for (Object o : objects)
			this.objects.add(new RDOFile(o));
	}
}
