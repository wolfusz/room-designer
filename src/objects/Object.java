package objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import GUI.MainFrame;

import java.awt.geom.Ellipse2D;

import utils.Globals;
import utils.Point;

public class Object {
	private Color color;
	private String name;
	private Point position;
	private int angle;
	private Shape shape;
	private boolean fixed;
	
	// CONSTRUCTORS
	public Object(int x, int y, String n, Shape s) {
		this.color = Color.BLACK;
		this.name = n;
		this.position = new Point(x,y);
		this.angle = 0;
		this.shape = s;
		this.fixed = false;
	}
	public Object(int x, int y, Shape s) { this(x,y,"",s); }
	public Object(int x, int y, String n) { this(x,y,n,null); }
	public Object(int x, int y) { this(x,y,""); }
	public Object() { this(0,0); }

	// clone object
	public Object(Object o) {
		this.name = ""+o.getName();
		this.fixed = o.isFixed();
		this.color = new Color(o.getColor().getRGB());
		this.angle = o.getAngle();
		this.position = new Point(o.getPosition());
		if (o.getShape() instanceof Rectangle2D) {
			Rectangle2D r = (Rectangle2D) o.getShape();
			this.shape = new Rectangle2D.Double(r.getX(), r.getY(), r.getWidth(), r.getHeight());
		} else if (o.getShape() instanceof Ellipse2D) {
			Ellipse2D r = (Ellipse2D) o.getShape();
			this.shape = new Ellipse2D.Double(r.getX(), r.getY(), r.getWidth(), r.getHeight());
		} else if (o.getShape() instanceof Polygon) {
			Polygon p = new Polygon();
			Polygon orig = (Polygon)o.getShape();
			for (int i = 0; i < orig.npoints; i++)
				p.addPoint(orig.xpoints[i], orig.ypoints[i]);
			this.shape = p;
		}
	}
	// load object from file
	public Object(RDOFile RDO) {
		this.color = RDO.getColor();
		this.name = RDO.getName();
		this.position = RDO.getPosition();
		this.shape = RDO.getShape();
		this.angle = RDO.getAngle();
		this.fixed = RDO.isFixed();
	}

	// GETTERS AND SETTERS
	public Point getPosition() { return this.position; }
	
	public int getMinX() { return (int)this.shape.getBounds().getMinX(); }
	public int getMinY() { return (int)this.shape.getBounds().getMinY(); }
	public int getMaxX() { return (int)this.shape.getBounds().getMaxX(); }
	public int getMaxY() { return (int)this.shape.getBounds().getMaxY(); }
	
	public void setName(String n) { this.name = n; }
	public String getName() { return this.name; }

	public double getAngleRadians() { return Math.toRadians(getAngle()); }
	public int getAngle() { return this.angle; }
	public void setAngle(int a) { this.angle = a % 360; }

	public void setColor(Color c) { this.color = c; }
	public Color getColor() { return this.color; }
	public void setShape(Shape s) { this.shape = s; }
	public Shape getShape() { return this.shape != null ? this.shape : new Rectangle2D.Double(-1,-1,3,3); }
	public Shape getTransformedShape() {
		AffineTransform transform = new AffineTransform();
		transform.translate(getPosition().getX(), getPosition().getY());
		transform.rotate(getAngleRadians());
		return transform.createTransformedShape(getShape());
	}
	
	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}
	public boolean isFixed() { return this.fixed; }
	
	// USEFUL
	public void move(int dx, int dy) {
		if (this.fixed == true) return;
		
		// set new position
		getPosition().set( getPosition().add(dx, dy) );
		
		// check if in window bounds
		Shape s = getTransformedShape();
		int minX = (int)s.getBounds2D().getMinX();
		int minY = (int)s.getBounds2D().getMinY();
		int maxX = (int)s.getBounds2D().getMaxX();
		int maxY = (int)s.getBounds2D().getMaxY();
		if ((minX < 0) || (maxX > Globals.Width) || (minY < 0) || (maxY > Globals.Height)) {
			Integer x = null;
			Integer y = null;
			if (minX<0 || maxX>Globals.Width) {
				x = (maxX-minX)/2;
				if (maxX>Globals.Width)
					x = Globals.Width - x;
			}
			if (minY<0 || maxY>Globals.Height) {
				y = (maxY-minY)/2;
				if (maxY>Globals.Height)
					y = Globals.Height - y;
			}
			
			// move object inside window
			if (x != null) getPosition().setX(x);
			if (y != null) getPosition().setY(y);
		}
	}
	
	// check if point within shape
	public boolean contains(Point p) { return getTransformedShape().contains(p.getX(), p.getY()); }
	
	public void select() {
		Globals.objects.setSelectedItem(this);
	}
	public void deselect() {
		Globals.objects.setSelectedItem(null);
		MainFrame.getInstance().displayPanel.selectedObject = null;
	}
	
	public String toString() {
		String ret = "";
		if (getShape() instanceof Rectangle2D)
			ret += "R: ";
		else if (getShape() instanceof Ellipse2D)
			ret += "E: ";
		else if (getShape() instanceof Polygon)
			ret += "P: ";
		return ret + getName();
	}
	
	// DRAWING
	public void draw(Graphics2D g) {
		AffineTransform temp = g.getTransform();
		
		g.setColor(getColor());		
		g.translate(getPosition().getX(), getPosition().getY());
		g.rotate(getAngleRadians());
		if (this.fixed == true)
			g.draw(getShape());
		else
			g.fill(getShape());
		
		g.setTransform(temp);
	}
}
