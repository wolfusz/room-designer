package GUI;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;
import javax.swing.Timer;
import javax.swing.UIManager;

import GUI.component.DisplayPanel;

public class MainFrame extends JFrame implements ActionListener {
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {}
		getInstance(); // init gui
	}

	// SINGLETON
	private static final long serialVersionUID = 7691407236548349576L;
	private static MainFrame instance;
	public static MainFrame getInstance() {
		if (instance == null)
			instance = new MainFrame();
		return instance;
	}
	
	// MAIN FRAME
	public DisplayPanel displayPanel = null;
	public ControlsWindow controlsWindow = null;
	
	private boolean maximized = false;
	private Point beforeMaximizationLocation = null;
	private Dimension beforeMaximizationSize = null;
	
	protected MainFrame() {
		this.setTitle("Room designer");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setMinimumSize(new Dimension(200, 200));
		
		this.addComponentListener(new ComponentAdapter() {
			@Override public void componentResized(ComponentEvent e) {
				if ((getExtendedState() & MAXIMIZED_BOTH) != MAXIMIZED_BOTH && !maximized) {
					beforeMaximizationLocation = getLocation();
					beforeMaximizationSize = getSize();
				}
			}
		});
		this.addWindowStateListener(new WindowStateListener() {
			@Override public void windowStateChanged(WindowEvent e) {
				if ((e.getNewState() & MAXIMIZED_BOTH) == MAXIMIZED_BOTH) {
					if (!maximized) {
						if (controlsWindow != null) {
							setSize(getWidth() - controlsWindow.getWidth(), getHeight());
							setLocation(controlsWindow.getWidth()-9, 0);
						}
					} else {
						setSize(beforeMaximizationSize);
						setLocation(beforeMaximizationLocation);
					}
					maximized = !maximized;
					setExtendedState(NORMAL);
				}
			}
		});

		displayPanel = new DisplayPanel();
		this.add(displayPanel);
		this.pack();
		
		controlsWindow = new ControlsWindow(this);
		setLocationRelativeTo(null);

		Timer timer = new Timer(10, this);
		this.setVisible(true);
		timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
		controlsWindow.repaint();
	}
	
}
