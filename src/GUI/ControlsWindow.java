package GUI;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JWindow;
import javax.swing.filechooser.FileFilter;

import GUI.component.Button;
import GUI.component.CheckBox;
import GUI.component.EditPanel;
import GUI.component.TextField;
import GUI.component.Tooltip;
import objects.Object;
import objects.RDLFile;
import objects.RDOFile;
import utils.Globals;

import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;

@SuppressWarnings("serial")
public class ControlsWindow extends JWindow implements ActionListener, ComponentListener, KeyListener {
	private static final long serialVersionUID = -6966488395120466316L;

	public Button bSave;
	public Button bLoad;
	public Button bClear;
	
	public Button bAdd;
	public Button bDel;
	public Button bClone;
	public Button bOSave;
	public Button bOLoad;
	
	public JTextArea credits;
	public TextField tName;
	public CheckBox cFixed;
	public Button bColor;
	
	public static JComboBox<String> editMode;
	public EditPanel editPanel;
	private JFrame parent;
	
	public ControlsWindow(JFrame frame) {
		super(frame);
		parent = frame;
		parent.addComponentListener(this);
		
		setMinimumSize(new Dimension(160, 250));
		((JComponent) getContentPane()).setBorder(BorderFactory.createLineBorder(Color.ORANGE, 2));
		
		JPanel panel = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2 = (Graphics2D)g.create();
				g2.setColor(Color.BLACK);
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
				g2.dispose();
			}
		};
		panel.setBorder(null);
		panel.setLayout(null);
		panel.setBackground(Color.ORANGE);
		
		// LAYOUT
		JLabel lLAYOUT = new JLabel("LAYOUT");
		lLAYOUT.setBounds(0, 0, 100, 13);
		lLAYOUT.setHorizontalAlignment(JLabel.CENTER);
		panel.add(lLAYOUT);

		bSave = new Button("lsave",false, 7,12, 26,26, this);
		bLoad = new Button("lload",false, 37,12, 26,26, this);
		bClear = new Button("lclear",false, 67,12, 26,26, this);
		panel.add(bSave);
		panel.add(bLoad);
		panel.add(bClear);
		
		credits = new JTextArea("Room Designer"+System.lineSeparator()+"     v1.0      by"+System.lineSeparator()+"              Dawid"+System.lineSeparator()+"            Bugajski");
		credits.setBounds(100, 0, 60, 48);
		credits.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 8));
		credits.setOpaque(false);
		credits.setEnabled(false);
		panel.add(credits);
		
		// OBJECT
		JLabel lOBJECT = new JLabel("OBJECT");
		lOBJECT.setBounds(10, 53, 50, 13);
		panel.add(lOBJECT);
		
		Globals.objects = new JComboBox<>();
		Globals.objects.setBounds(50, 50, 100, 20);
		Globals.objects.addActionListener(this);
		Globals.objects.setBackground(null);
		panel.add(Globals.objects);
		Tooltip.add(Globals.objects, new Tooltip(){ @Override public String getText() {
			if (Globals.objects.getSelectedItem() != null) {
				Shape shape = ((Object)Globals.objects.getSelectedItem()).getShape();
				if (shape instanceof Rectangle2D) {
					Rectangle2D r = (Rectangle2D)shape;
					return String.format("Rectangle [x=%s; y=%s; w=%s; h=%s]", r.getX(), r.getY(), r.getWidth(), r.getHeight());
				}
				else if (shape instanceof Ellipse2D) {
					Ellipse2D r = (Ellipse2D)shape;
					return String.format("Ellipse [rx=%s; ry=%s]", r.getWidth(), r.getHeight());
				}
			}
			return "";
		}});

		bAdd = new Button("add",false, 7,72, 26,26, this);
		bDel = new Button("del",false, 37,72, 26,26, this);
		bClone = new Button("clone",true, 67,72, 26,26, this);
		bOSave = new Button("save",true, 97,72, 26,26, this);
		bOLoad = new Button("load",true, 127,72, 26,26, this);
		
		bOSave.setBackground(Color.BLUE);
		bOLoad.setBackground(Color.BLUE);

		panel.add(bOSave);
		panel.add(bOLoad);
		panel.add(bAdd);
		panel.add(bDel);
		panel.add(bClone);

		tName = new TextField("Name:",10, 7,100, 85,26, this);
		bColor = new Button("color",true, 97,100, 26,26, this);
		bColor.setBackground(Color.BLACK);
		cFixed = new CheckBox("fixed",true, false, 127,100, 26,26, this);
		
		panel.add(tName);
		panel.add(bColor);
		panel.add(cFixed);
				
		Tooltip.add(tName, new Tooltip(){ @Override public String getText() { return "Press \"<b>ENTER</b>\" to save changes."; } });
		
		editMode = new JComboBox<>();
		editMode.setBounds(0, 130, 155, 20);
		editMode.addItem("Edit mode: rectangle");
		editMode.addItem("Edit mode: ellipse");
		editMode.addItem("Edit mode: polygon");
		editMode.addActionListener(this);
		panel.add(editMode);
		
		
		editPanel = new EditPanel();
		editPanel.setBackground(Color.WHITE);
		editPanel.setBounds(0, 150, 155, 100);
		panel.add(editPanel);
		
		getContentPane().add(panel, BorderLayout.CENTER);
		pack();

		fileDialog = new JFileChooser();
		
		setVisible(true);
		reposition();
	}
	
	
	// EVENT LISTENERS
	// Buttons
	@Override
	public void actionPerformed(ActionEvent e) {
		/*
		// LAYOUT SAVING, LOADING AND CLEARING
		*/
		if (e.getSource().equals(bSave)) {
			if (Globals.objects.getItemCount() > 0) {
				// SHOW LAYOUT SAVING DIALOG
				prepareDialog("Where do you want to save this layout?",RDLFileFilter, "untitled layout.rdl");
				if (fileDialog.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
					File f = checkFile( fileDialog.getSelectedFile(), "rdl" );
					try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(f))) {
						Point pos = MainFrame.getInstance().getLocation();
						Dimension size = MainFrame.getInstance().getSize();
						LinkedList<Object> objects = new LinkedList<>();
						for (int i = 0; i < Globals.objects.getItemCount(); i++)
							objects.add( Globals.objects.getItemAt(i) );
						os.writeObject( new RDLFile(pos.x, pos.y, size.width, size.height, objects) );
						os.close();
					} catch (Exception e1) {}
				}
			}
		}
		else if (e.getSource().equals(bLoad)) {
			// SHOW LAYOUT LOADING DIALOG
			prepareDialog("Choose layout to load:",RDLFileFilter,"");
			if (fileDialog.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				File f = checkFile( fileDialog.getSelectedFile(), "rdl" );
				try (ObjectInputStream os = new ObjectInputStream(new FileInputStream(f))) {
					// load layout from file
					RDLFile RDL = (RDLFile) os.readObject();
					// set window position and size
					MainFrame.getInstance().setLocation(RDL.getX(), RDL.getY());
					MainFrame.getInstance().setSize(new Dimension(RDL.getWidth(), RDL.getHeight()));
					// clear current list and add all layout objects
					Globals.objects.removeAllItems();
					for (Object o : RDL.getObjects())
						Globals.objects.addItem(o);
					os.close();
				} catch (Exception e1) {}
			}
		}
		else if (e.getSource().equals(bClear)) {
			// clear current list and add all layout objects
			Globals.objects.removeAllItems();
		}
		
		/*
		// OBJECT SAVING AND LOADING
		*/
		else if (e.getSource().equals(bOSave)) {
			if (Globals.objects.getSelectedItem() != null) {
				Object o = (Object) Globals.objects.getSelectedItem();
				// SHOW OBJECT SAVING DIALOG
				prepareDialog("Where do you want to save this object?",RDOFileFilter, (o.getName().isEmpty() ? "unnamed" : o.getName())+".rdo");
				if (fileDialog.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
					File f = checkFile( fileDialog.getSelectedFile(), "rdo" );
					try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(f))) {
						os.writeObject(new RDOFile(o));
						os.close();
					} catch (Exception e1) {}
				}	
			}
		}
		else if (e.getSource().equals(bOLoad)) {
			// SHOW OBJECT LOADING DIALOG
			prepareDialog("Choose object to load:",RDOFileFilter,"");
			if (fileDialog.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				File f = checkFile( fileDialog.getSelectedFile(), "rdo" );
				try (ObjectInputStream os = new ObjectInputStream(new FileInputStream(f))) {
					Object object = new Object((RDOFile) os.readObject());
					Globals.objects.addItem(object);
					Globals.objects.setSelectedItem(object);
					MainFrame.getInstance().displayPanel.resetSelectedObjectsPosition();
					os.close();
				} catch (Exception e1) {}
			}
		}

		/*
		// OBJECT EDITING
		*/
		else if (e.getSource().equals(bAdd)) {
			// ADD NEW ITEM AND SELECT IT
			Object object = new Object(Globals.Width/2, Globals.Height/2);
			switch (editPanel.currentEditMode) {
			case ELLIPSE: object.setShape( new Ellipse2D.Double(-1, -1, 3, 3) ); break;
			case POLY: object.setShape( new Polygon() ); break;
			default: object.setShape( new Rectangle2D.Double(-1, -1, 3, 3) );
			}
			Globals.objects.addItem(object);
			Globals.objects.setSelectedItem(object);
		}
		else if (e.getSource().equals(bDel)) {
			// DELETE SELECTED ITEM
			if (Globals.objects.getSelectedItem() != null)
				Globals.objects.removeItem( Globals.objects.getSelectedItem() );
		}
		else if (e.getSource().equals(bClone)) {
			// ADD NEW ITEM FROM SELECTED
			if (Globals.objects.getSelectedItem() != null) {
				Globals.objects.addItem(new Object((Object) Globals.objects.getSelectedItem()));
				Globals.objects.setSelectedIndex(Globals.objects.getItemCount()-1);
			}
		}
		else if (e.getSource().equals(bColor)) {
			if (Globals.objects.getSelectedItem() != null) {
				Color color = JColorChooser.showDialog(null, "Choose object color:", MainFrame.getInstance().displayPanel.selectedObject.getColor());
				if (color != null) {
					((Object) Globals.objects.getSelectedItem()).setColor(color);
					bColor.setBackground(color);
				}
			}
		}
		else if (e.getSource().equals(cFixed)) {
			if (Globals.objects.getSelectedItem() != null) {
				Object o = (Object) Globals.objects.getSelectedItem(); 
				o.setFixed(cFixed.isSelected());
				if (o.isFixed()) {
					Globals.objects.removeItem(o);
					Globals.objects.insertItemAt(o, 0);
					Globals.objects.setSelectedIndex(0);
				} else {
					Globals.objects.removeItem(o);
					Globals.objects.addItem(o);
					Globals.objects.setSelectedItem(o);
				}
			} else cFixed.setSelected(! cFixed.isSelected());
		}
		
		// COMBOBOXES
		else if (e.getSource().equals(Globals.objects)) {
			Object o = (Object)Globals.objects.getSelectedItem();
			if (o != null) {
				editPanel.polyPoints.clear();
				editPanel.selectedPolyIndex = -1;
				if (o.getShape() instanceof Polygon) {
					Polygon poly = (Polygon)o.getShape();
					for (int i = 0; i < poly.npoints; i++)
						editPanel.polyPoints.add(new utils.Point(poly.xpoints[i], poly.ypoints[i]));
				}
				bColor.setBackground(o.getColor());
				cFixed.setSelected(o.isFixed());
				editPanel.setObjectAndEditMode(o);
				tName.setText(o.getName());
			}
		}
		else if (e.getSource() == editMode)
			editPanel.setChangeEditMode(editMode.getSelectedIndex());
	}
	
	// Textfield
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getSource() == tName) {
			if (! tName.isEnabled()) return;
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				Object o = (Object)Globals.objects.getSelectedItem();
				if (o != null)
					o.setName(tName.getText());
				repaint();
			}
		}
	}


	// HOLDING WINDOW NEAR PARENT
	public void reposition() {
		if (parent == null) return;
		int x = parent.getLocation().x - this.getWidth() + 7;
		int y = parent.getLocation().y;
		this.setLocation(x, y);
	}
	@Override
	public void componentMoved(ComponentEvent e) { reposition(); }
	@Override
	public void componentResized(ComponentEvent e) { reposition(); }
	
	
	// FILE CHOOSING DIALOG
	private final JFileChooser fileDialog;
	
	// useful functions
	public void prepareDialog(String title, FileFilter filter, String defaultFile) {
		fileDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileDialog.setDialogTitle(title);
		fileDialog.setCurrentDirectory( new File(System.getProperty("user.dir")) );
		fileDialog.resetChoosableFileFilters();
		fileDialog.setAcceptAllFileFilterUsed(false);
		fileDialog.setFileFilter(filter);
		fileDialog.setSelectedFile(new File(defaultFile));
	}
	
	public File checkFile(File f, String ext) {
		String path = f.getAbsolutePath();
		if (! path.endsWith("."+ext))
			f = new File(path+"."+ext);
		return f;
	}
	
	public String getExtension(String s) {
		int i = s.lastIndexOf(".");
		if (i>0 && i<s.length()-1)
			return s.substring(i+1).toLowerCase();
		return "";
	}
	
	// file filters
	private final FileFilter RDLFileFilter = new FileFilter() {
		@Override
		public String getDescription() { return "Room Designer Layout | *.rdl"; }
		
		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) return true;
			return getExtension(f.getName()).equals("rdl");
		}
	};
	private final FileFilter RDOFileFilter = new FileFilter() {
		@Override
		public String getDescription() { return "Room Designer Object | *.rdo"; }
		
		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) return true;
			return getExtension(f.getName()).equals("rdo");
		}
	};
	
	
	// UNUSED
	@Override
	public void componentHidden(ComponentEvent e) {}
	@Override
	public void componentShown(ComponentEvent e) {}
	@Override
	public void keyTyped(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {}
}
