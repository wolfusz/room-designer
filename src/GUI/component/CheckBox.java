package GUI.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;

@SuppressWarnings("serial")
public class CheckBox extends JCheckBox {
	
	private ImageIcon icon;
	private boolean fitIcon;
	
	public CheckBox(String icon, boolean fitIcon, boolean checked, int x, int y, int w, int h, ActionListener actionListener) {
		super("", checked);
		setBounds(x, y, w, h);
		setContentAreaFilled(false);
		setOpaque(false);
		setBorderPainted(false);
		setFocusPainted(false);
		this.icon = new ImageIcon(getClass().getResource("/resources/"+icon+".png"));
		this.fitIcon = fitIcon;
		setBackground(Color.WHITE);
		addActionListener(actionListener);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D canvas = (Graphics2D)g;
		
		canvas.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		canvas.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		
		if (isSelected())
			canvas.setColor(Color.BLACK);
		else
			canvas.setColor(getBackground());
		canvas.fillOval(0, 0, getWidth()-1, getHeight()-1);

		if (fitIcon) {
			int w = getWidth()/6;
			int h = getHeight()/6;
			canvas.drawImage(this.icon.getImage(), w, h, getWidth()-2*w, getHeight()-2*h, null);
		} else
			canvas.drawImage(this.icon.getImage(), 0, 0, getWidth(), getHeight(), null);

		if (getModel().isRollover())
			canvas.setColor(Color.WHITE);
		else
			canvas.setColor(Color.GRAY);
		canvas.drawOval(0, 0, getWidth()-1, getHeight()-1);
		
		canvas.dispose();
	}
}
