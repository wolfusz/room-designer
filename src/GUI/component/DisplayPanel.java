package GUI.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;

import javax.swing.JPanel;

import objects.Object;
import utils.Globals;
import utils.Point;
import utils.Utils;

public class DisplayPanel extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener {
	private static final long serialVersionUID = -8108647542207715609L;
	
	Tooltip tooltip;
	public DisplayPanel() {
        setPreferredSize(new Dimension(300, 300));
        
        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
        
        addComponentListener(new ComponentAdapter(){
        	@Override
        	public void componentResized(ComponentEvent e) {
        		super.componentResized(e);
				Globals.Width = getWidth();
				Globals.Height = getHeight();
        	}
        });
        
        tooltip = new Tooltip(){ @Override public String getText() {
        	String tooltip;
        	if (selectedObject != null) {
        		tooltip = "Object position: "+selectedObject.getPosition().toString();
        		tooltip += "<br>Object angle: "+selectedObject.getAngle();
        		tooltip += "<br>Scale: "+Utils.NRound(scale,3);
        	} else 
        		tooltip = "Scale: "+Utils.NRound(scale,3);
        	return tooltip;
    	}};
        Tooltip.add(this, tooltip);
	}

	double scale = 1.0;
	public Object selectedObject = null;
	
	// DRAWING
	@Override
    protected void paintComponent(Graphics g) {
		Graphics2D canvas = (Graphics2D)g.create();
		
		canvas.setColor(Color.WHITE);
		canvas.fillRect(0, 0, getWidth(), getHeight());
		
		canvas.scale(scale, scale);
		
		if (Globals.objects != null)
			for (int i = 0; i < Globals.objects.getItemCount(); i++)
	    		((Object)Globals.objects.getItemAt(i)).draw(canvas);
    	
    	if (selectedObject != null) {
    		canvas.setColor(new Color(0x55FF00));
    		
    		AffineTransform temp = canvas.getTransform();
    		canvas.translate(selectedObject.getPosition().getX(), selectedObject.getPosition().getY());
    		canvas.rotate(selectedObject.getAngleRadians());
    		canvas.draw(selectedObject.getShape());
    		canvas.rotate(-selectedObject.getAngleRadians());
    		canvas.setColor(Color.RED);
    		canvas.drawLine(-3, 0, 3, 0);
    		canvas.drawLine(0, -3, 0, 3);
    		canvas.setTransform(temp);
    	}
    }
	
	// MOUSE CONTROLS
	private Point mousePosition = null;
	private boolean mouseDragging = false;

	public void resetSelectedObjectsPosition() {
		if (selectedObject != null)
			selectedObject.getPosition().set(getWidth()/2, getHeight()/2);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		mousePosition = new Point((int)(e.getX()*scale), (int)(e.getY()*scale)); // MOUSE POSITION
		if (e.getButton() == MouseEvent.BUTTON1) { // LEFT MOUSE BUTTON
			// deselect object
			if (selectedObject != null)
				selectedObject.deselect();
			
			// check if clicked object
			for (int i = Globals.objects.getItemCount()-1; i >= 0; i--) {
				Object o = Globals.objects.getItemAt(i);
				if (o.contains(mousePosition)) {
					mouseDragging = true;
					o.select(); // SET OBJECT AS CURRENTLY SELECTED
					tooltip.repaint();
					break;
				}
			}
		} else if (e.getButton() == MouseEvent.BUTTON2) { // MIDDLE MOUSE BUTTON
			// reset current objects position to center
			if ((e.getModifiers()&MouseWheelEvent.CTRL_MASK)>0) {
				scale = 1;
				tooltip.repaint(true);
			} else
				resetSelectedObjectsPosition();
		} else if (e.getButton() == MouseEvent.BUTTON3) { // RIGHT MOUSE BUTTON
			// reset current objects angle
			selectedObject.setAngle(0);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		mouseDragging = false;
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if (mousePosition != null && mouseDragging == true && Globals.objects.getSelectedItem() != null) {
			Point currentPosition = new Point((int)(e.getX()*scale), (int)(e.getY()*scale));
			int dx = currentPosition.getX() - mousePosition.getX();
			int dy = currentPosition.getY() - mousePosition.getY();
			((Object) Globals.objects.getSelectedItem()).move(dx, dy);
			tooltip.repaint();
		}
		mousePosition = new Point((int)(e.getX()*scale), (int)(e.getY()*scale));
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if ((e.getModifiers()&MouseWheelEvent.CTRL_MASK)>0) {
			// CHANGE EDIT VIEW ZOOM
			scale += -0.01 * e.getWheelRotation();
			scale = Math.abs(scale);
			tooltip.repaint(true);
		} else if (Globals.objects.getSelectedItem() != null) {
			int rotation = (-e.getWheelRotation())*2;
			Object o = (Object)Globals.objects.getSelectedItem();
			if (! o.isFixed())
				o.setAngle( o.getAngle()+rotation );
			tooltip.repaint();
		}
	}
	
	// UNUSED
	@Override
	public void mouseMoved(MouseEvent e) {}
	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent arg0) {}
	@Override
	public void mouseExited(MouseEvent arg0) {}
}
