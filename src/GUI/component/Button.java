package GUI.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class Button extends JButton {

	private ImageIcon icon;
	private boolean fitIcon;
	
	public Button(String icon, boolean fitIcon, int x, int y, int w, int h, ActionListener actionListener) {
		super("");
		setBounds(x, y, w, h);
		setContentAreaFilled(false);
		setOpaque(false);
		setBorderPainted(false);
		setFocusPainted(false);
		
		this.icon = null;
		if (!icon.equals(""))
			this.icon = new ImageIcon(getClass().getResource("/resources/"+icon+".png"));
		this.fitIcon = fitIcon;
		
		setBackground(Color.WHITE);
		addActionListener(actionListener);
	}
	public Button(String icon, int x, int y, int w, int h, ActionListener actionListener) {
		this(icon,false, x,y, w,h, actionListener);
	}
	
	public ImageIcon getIconPath() { return icon; }
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D canvas = (Graphics2D)g;
		
		canvas.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		canvas.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		canvas.setColor(getBackground());
		canvas.fillOval(0, 0, getWidth()-1, getHeight()-1);

		if (icon != null) {
			if (fitIcon) {
				int w = getWidth()/6;
				int h = getHeight()/6;
				canvas.drawImage(this.icon.getImage(), w, h, getWidth()-2*w, getHeight()-2*h, null);
			} else
				canvas.drawImage(this.icon.getImage(), 0, 0, getWidth(), getHeight(), null);
		}

		if (getModel().isPressed()) {
			canvas.setColor(Color.BLACK);
		} else if (getModel().isRollover())
			canvas.setColor(Color.WHITE);
		else
			canvas.setColor(Color.GRAY);
		canvas.drawOval(0, 0, getWidth()-1, getHeight()-1);
		
		canvas.dispose();
	}
}
