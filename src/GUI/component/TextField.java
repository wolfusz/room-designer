package GUI.component;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.event.KeyListener;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JTextField;

@SuppressWarnings("serial")
public class TextField extends JTextField {

    private Shape shape;
    private String title;
	public TextField(String title, int cols, int x, int y, int w, int h, KeyListener keyListener) {
		super("");
		this.title = title;
		setOpaque(false);
		setBounds(x,y,w,h);
		setColumns(cols);
		addKeyListener(keyListener);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
        g.setColor(getBackground());
        g.fillRoundRect(0, 0, getWidth(), getHeight()-1, getHeight()-1, getHeight());
        super.paintComponent(g);
	}

	@Override
	protected void paintBorder(Graphics g) {
        g.setColor(getBackground());
        g.drawRoundRect(0, 0, getWidth()-1, getHeight()-1, getHeight(), getHeight());
        if (getText().equals("")) {
        	g.setColor(Color.GRAY);
        	g.setFont(new Font(Font.SANS_SERIF,Font.PLAIN,(int)(getHeight()*0.4)));
        	g.drawString(title, 5, (int)(getHeight()*0.7));
        }
	}
	
	@Override
    public boolean contains(int x, int y) {
        if (shape == null || !shape.getBounds().equals(getBounds()))
            shape = new RoundRectangle2D.Float(0, 0, getWidth()-1, getHeight()-1, 15, 15);
        return shape.contains(x, y);
   }
}
