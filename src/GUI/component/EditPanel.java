package GUI.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;

import javax.swing.JPanel;

import GUI.ControlsWindow;
import GUI.MainFrame;
import objects.Object;
import utils.Globals;
import utils.Point;
import utils.Utils;

@SuppressWarnings("serial")
public class EditPanel extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener {

	public Object object;
	public double scale = 1.0;
	
	public EditMode currentEditMode = EditMode.RECT;
	public int selectedPolyIndex = -1;
	public LinkedList<Point> polyPoints;
	
	public enum EditMode { RECT, ELLIPSE, POLY }
	public String getCurrentModeText() {
		switch (currentEditMode) {
		case RECT: return "rectangle";
		case ELLIPSE: return "ellipse / circle";
		case POLY: return "polygon";
		default: return "unknown";
		}
	}
	public void setChangeEditMode(int i) {
		currentEditMode = EditMode.values()[i];
		if (object != null) {
			switch (currentEditMode) {
			case RECT:
				object.setShape( new Rectangle2D.Double(-1, -1, 3, 3) );
				break;
			case ELLIPSE:
				object.setShape( new Ellipse2D.Double(-1, -1, 3, 3) );
				break;
			case POLY:
				polyPoints.clear();
				object.setShape( new Polygon() );
				break;
			default: break;
			}
		}
		tooltip.show();
	}
	public void changeEditMode(int i) {
		currentEditMode = EditMode.values()[ (currentEditMode.ordinal()+i+EditMode.values().length) % EditMode.values().length ];
		if (object != null) {
			switch (currentEditMode) {
			case RECT:
				object.setShape( new Rectangle2D.Double(-1, -1, 3, 3) );
				break;
			case ELLIPSE:
				object.setShape( new Ellipse2D.Double(-1, -1, 3, 3) );
				break;
			case POLY:
				object.setShape( new Polygon() );
				break;
			default: break;
			}
		}
		tooltip.show();
	}
	public void changeEditMode(Shape shape) {
		ControlsWindow.editMode.removeActionListener(MainFrame.getInstance().controlsWindow);
		if (shape instanceof Rectangle2D) {
			currentEditMode = EditMode.RECT;
			ControlsWindow.editMode.setSelectedIndex(0);
		} else if (shape instanceof Ellipse2D) {
			currentEditMode = EditMode.ELLIPSE;
			ControlsWindow.editMode.setSelectedIndex(1);
		} else if (shape instanceof Polygon) {
			currentEditMode = EditMode.POLY;
			ControlsWindow.editMode.setSelectedIndex(2);
		}
		ControlsWindow.editMode.addActionListener(MainFrame.getInstance().controlsWindow);
	}
	
	public void setObjectAndEditMode(Object o) {
		this.object = o;
		MainFrame.getInstance().displayPanel.selectedObject = o;
		if (o != null)
			changeEditMode(o.getShape());
	}
	
	Tooltip tooltip = null;
	public EditPanel() {
		addMouseListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);
		polyPoints = new LinkedList<>();
		
		tooltip = new Tooltip(){
			@Override
			public String getText() {
				String textToShow = "";
				if (object != null) {
					textToShow += "<b>Current zoom:</b><i> "+ Utils.NRound(scale,3) +"</i>"; 
					textToShow += "<br><b>Current drawing mode:</b> "+getCurrentModeText();
					textToShow += "<br><b>Mouse Wheel:</b><i> change zoom</i>";
					textToShow += "<br><b>Ctrl+Mouse Wheel:</b><i> reset zoom</i><br>";
					switch (currentEditMode) {
					case RECT:
						textToShow += "<br><b>Mouse Left:</b><i> select and move vertex to change<br>rectangle width and height</i>";
						textToShow += "<br><i>(hold shift to keep aspect ratio)</i>";
						break;
					case ELLIPSE:
						textToShow += "<br><b>Mouse Left:</b><i> change X and Y radius relatively</i>";
						textToShow += "<br><i>(hold shift to keep aspect ratio)</i>";
						break;
					case POLY:
						textToShow += "<br><b>Mouse Left:</b><i> select/move vertex</i>";
						textToShow += "<br><b>Mouse Middle:</b><i> delete clicked vertex</i>";
						textToShow += "<br><b>Mouse Right:</b><i> add vertex</i>";
						break;
					}
				} else {
					textToShow = "Click <img src=\""+MainFrame.getInstance().controlsWindow.bAdd.getIconPath()+"\"/> button to add item.";
				}
				return textToShow;
			}
		};
		Tooltip.add(this, tooltip);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Graphics2D canvas = (Graphics2D)g.create();
		AffineTransform temp = canvas.getTransform();
		
		canvas.setColor(Color.WHITE);
		canvas.fill(getBounds());
		
		canvas.translate(getWidth()/2, getHeight()/2);
		canvas.scale(scale, scale);
		canvas.setColor(Color.BLACK);
		
		if (object != null) {
			canvas.draw(object.getShape());
			
			if (currentEditMode == EditMode.POLY) {
				// draw polygon center
				canvas.setColor(Color.BLUE);
				canvas.drawLine(-3, 0, -1, 0);
				canvas.drawLine(+1, 0, +3, 0);
				canvas.drawLine(0, -3, 0, -1);
				canvas.drawLine(0, +1, 0, +3);
				// draw points
				for (Point p : polyPoints)
					canvas.fillOval(p.getX()-2, p.getY()-2, 5, 5);
				// draw selected point
				canvas.setColor(Color.RED);
				if (selectedPolyIndex > -1 && selectedPolyIndex < polyPoints.size()) {
					int x = polyPoints.get(selectedPolyIndex).getX();
					int y = polyPoints.get(selectedPolyIndex).getY();
					canvas.drawLine(x-3, y, x-1, y);
					canvas.drawLine(x+1, y, x+3, y);
					canvas.drawLine(x, y-3, x, y-1);
					canvas.drawLine(x, y+1, x, y+3);
				}
				canvas.setColor(Color.BLACK);
			}
		}
		
		canvas.setTransform(temp);

		canvas.drawString("scale: "+Utils.NRound(scale,3), 0, 10);
		if (object != null) {
			canvas.drawString("x: "+(int)object.getShape().getBounds().getWidth(), 0, 20);
			canvas.drawString("y: "+(int)object.getShape().getBounds().getHeight(), 0, 30);
		}
	}



	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// IF HOLDING CTRL
		if ((e.getModifiers()&MouseWheelEvent.CTRL_MASK)>0) {
			// reset zoom
			scale = 1;
		} else {
			scale += -0.01 * e.getWheelRotation();
			scale = Math.abs(scale);
		}
		
		/*// IF HOLDING CTRL
		if ((e.getModifiers()&MouseWheelEvent.CTRL_MASK)>0) {
			// CHANGE EDIT VIEW ZOOM
			scale += -0.01 * e.getWheelRotation();
			scale = Math.abs(scale);
		} else {
			// CHANGE EDIT MODE
			if (e.getWheelRotation() > 0) {
				changeEditMode(+1);
			} else if (e.getWheelRotation() < 0) {
				changeEditMode(-1);
			}
		}*/
	}

	
	private Point mousePosition = null;
	private boolean dragging = false;
	
	@Override
	public void mousePressed(MouseEvent e) {
		mousePosition = new Point((int)(e.getX()*scale), (int)(e.getY()*scale));
		dragging = false;
		
		if (currentEditMode == EditMode.POLY && object != null && object.getShape() instanceof Polygon) {
			Point clickedPoint = mousePosition.sub(getWidth()/2, getHeight()/2);
			double maxDist = (5*(2-scale));
			
			switch (e.getButton()) {
			// LEFT MOUSE
			case MouseEvent.BUTTON1: {
				selectedPolyIndex = -1;
				if (!polyPoints.isEmpty()) {
					for (int i = 0; i < polyPoints.size(); i++)
						if (polyPoints.get(i).mul(scale).distance(clickedPoint) < maxDist) {
							selectedPolyIndex = i;
							break;
						}
					dragging = true;
				}
			}break;
			
			// MIDDLE MOUSE
			case MouseEvent.BUTTON2: {
				selectedPolyIndex = -1;
				if (!polyPoints.isEmpty()) {
					Polygon shape = ((Polygon)object.getShape());
					shape.reset();
					if (polyPoints.size() > 0) {
						for (int i = 0; i < polyPoints.size(); i++)
							if (polyPoints.get(i).distance(clickedPoint) < maxDist) {
								selectedPolyIndex = i;
								break;
							}
						System.out.println(selectedPolyIndex);
						if (selectedPolyIndex > -1 && selectedPolyIndex < polyPoints.size())
							polyPoints.remove(selectedPolyIndex);
						for (Point p : polyPoints)
							shape.addPoint(p.getX(), p.getY());
					} else
						polyPoints.clear();
				}
			}break;
			
			// RIGHT MOUSE
			case MouseEvent.BUTTON3: {
				// ADD POINT AT CURRENT
				if (selectedPolyIndex < 0 || selectedPolyIndex >= polyPoints.size()-1)
					polyPoints.add(clickedPoint);
				else
					polyPoints.add(selectedPolyIndex, clickedPoint);
				selectedPolyIndex = polyPoints.indexOf(clickedPoint);

				Polygon shape = ((Polygon)object.getShape());
				shape.reset();
				for (Point p : polyPoints)
					shape.addPoint(p.getX(), p.getY());
			}break;
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		dragging = false;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (mousePosition != null && object != null && Globals.objects.getSelectedItem() != null) {
			switch (currentEditMode) {
			default: break;
			
			case RECT:
			case ELLIPSE: {
				Point currentPosition = new Point((int)(e.getX()*scale), (int)(e.getY()*scale));
				int dx = currentPosition.getX() - mousePosition.getX();
				int dy = currentPosition.getY() - mousePosition.getY();
				if (e.getX() < getWidth()/2)
					dx = -dx;
				if (e.getY() < getHeight()/2)
					dy = -dy;

				Shape shape = object.getShape();
				Rectangle r = shape.getBounds();
				r.grow(dx, (e.getModifiers()&MouseEvent.SHIFT_MASK)>0 ? dx : dy);
				switch (currentEditMode) { default: break;
				case RECT: ((Rectangle2D)shape).setFrame(r); break;
				case ELLIPSE: ((Ellipse2D)shape).setFrame(r); break;
				}
				object.setShape(shape);
			}break;
			
			case POLY:{
				if (dragging && selectedPolyIndex > -1 && selectedPolyIndex < polyPoints.size()) {
					mousePosition = new Point((int)(e.getX()*scale)-getWidth()/2, (int)(e.getY()*scale)-getHeight()/2);
					Point currentPosition = polyPoints.get(selectedPolyIndex).mul(scale);
					int dx = mousePosition.getX() - currentPosition.getX();
					int dy = mousePosition.getY() - currentPosition.getY();
					polyPoints.get(selectedPolyIndex).set( polyPoints.get(selectedPolyIndex).add(dx, dy) );
					
					Polygon shape = (Polygon)object.getShape();
					shape.reset();
					for (Point p : polyPoints)
						shape.addPoint(p.getX(), p.getY());
				}
			}break;
			
			
			}
			
			Object o = (Object) Globals.objects.getSelectedItem();
			o.setShape(object.getShape());
		}
		mousePosition = new Point((int)(e.getX()*scale), (int)(e.getY()*scale));
	}

	
	
	
	// UNUSED
	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseMoved(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
}
