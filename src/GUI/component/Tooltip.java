package GUI.component;

import java.awt.Color;
import java.awt.Font;
import java.awt.Window.Type;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JWindow;

public class Tooltip extends MouseAdapter {

	// Tooltip handling
	public static void add(JComponent c, Tooltip t) {
		c.addMouseListener(t);
		t.setParent(c);
	}
	
	public static Tooltip get(JComponent c) {
		for (MouseListener ml : c.getMouseListeners())
			if (ml instanceof Tooltip)
				return (Tooltip)ml;
		return null;
	}

	
	// VARIABLES
	private JWindow tooltip = null;
	private JComponent parent = null;
	private String msg = "";
	
	// CONSTRUCTOR
	public Tooltip(String text) { msg = text; }
	public Tooltip() { this(""); }
	
	// SETTERS / GETTERS
	public void setText(String text) { msg = text; }
	public String getText() { return msg; }

	private void setParent(JComponent c) { this.parent = c; }
	
	// SHOWING
	public void show() {
		mouseExited(null);
		mouseEntered(null);
	}

	public void repaint(boolean resize) {
		textPanel.setText(getText());
		if (resize == true)
			tooltip.pack();
	}
	public void repaint() { repaint(false); }
	
	// MOUSE EVENTS
	JEditorPane textPanel = null;
	@Override
	public void mouseEntered(MouseEvent e) {
		if (!getText().equals("")) {
			if (tooltip != null)
				tooltip.dispose();
			
			tooltip = new JWindow();
			tooltip.setAlwaysOnTop(true);
			tooltip.setFocusable(false);
			tooltip.setBackground(Color.BLACK);
			tooltip.setType(Type.POPUP);
			
			textPanel = new JEditorPane("text/html", "");
			textPanel.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			textPanel.setEditable(false);
			textPanel.setBorder( BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(Color.BLACK, 1, true), // BORDER
				BorderFactory.createEmptyBorder(2, 4, 4, 4)          // MARGIN
			));
			textPanel.setText(getText());
			
			tooltip.add(textPanel);
			tooltip.pack();
			
			tooltip.setLocation(this.parent.getLocationOnScreen().x+5, this.parent.getLocationOnScreen().y-tooltip.getHeight()-5);
			tooltip.setVisible(true);
		}
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
		if (tooltip != null)
			tooltip.dispose();
		tooltip = null;
	}
}
